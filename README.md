# ProjetoJogo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.6, Node version 14.19.0.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## API server

Run `npm run startserver` for a API server.
