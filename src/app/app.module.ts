import { BrowserModule } from '@angular/platform-browser';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { IniciarJogoComponent } from './iniciar-jogo/iniciar-jogo.component';
import { RegrasJogoComponent } from './regras-jogo/regras-jogo.component';
import { RankingComponent } from './ranking/ranking.component';
import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

registerLocaleData(localePt, 'pt')

@NgModule({
  declarations: [AppComponent,HomeComponent,IniciarJogoComponent,RegrasJogoComponent,RankingComponent],
  imports: [BrowserModule,FormsModule,AppRoutingModule,HttpClientModule],
  providers: [
    {provide: LOCALE_ID, useValue: 'pt' },
    {
      provide: DEFAULT_CURRENCY_CODE,
      useValue: 'BRL',
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
