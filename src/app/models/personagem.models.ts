export interface Personagem{
  jogadorAtivo: number;
  nome: string;
  vida: number;
  ataqueEspecial: number;
  minAtaque: number;
  maxAtaque: number;
  minAtaqueEspecial: number;
  maxAtaqueEspecial: number;
  minCura?: number;
  maxCura?: number;
  atordoamento?: number
}
