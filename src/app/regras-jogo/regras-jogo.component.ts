import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RankingService } from '../services/ranking.service';

@Component({
  selector: 'app-regras-jogo',
  templateUrl: './regras-jogo.component.html',
  styleUrls: ['./regras-jogo.component.scss']
})
export class RegrasJogoComponent implements OnInit {

  constructor(
    private router: Router,
    private service: RankingService
  ){}

  botaoHome(){
    this.router.navigateByUrl('home');
  }

  ngOnInit(): void {
  }

}
