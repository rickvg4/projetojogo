import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { IniciarJogoComponent } from './iniciar-jogo/iniciar-jogo.component';
import { RankingComponent } from './ranking/ranking.component';
import { RegrasJogoComponent } from './regras-jogo/regras-jogo.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'iniciar-jogo', component: IniciarJogoComponent},
  { path: 'regras-jogo', component: RegrasJogoComponent},
  { path: 'ranking', component: RankingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
