import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogAcoes } from '../models/log-acoes.models';
import { Personagem } from '../models/personagem.models';
import { Ranking } from '../models/ranking.models';
import { RankingService } from '../services/ranking.service';


@Component({
  selector: 'app-iniciar-jogo',
  templateUrl: './iniciar-jogo.component.html',
  styleUrls: ['./iniciar-jogo.component.scss']
})
export class IniciarJogoComponent implements OnInit {

  constructor(
    private router: Router,
    private service: RankingService
  ){}

  log: LogAcoes[] = [];

  jogador: Personagem = {
    jogadorAtivo: 1,
    nome: 'Jogador',
    vida: 100,
    ataqueEspecial: 2,
    minAtaque: 5,
    maxAtaque: 10,
    minAtaqueEspecial: 10,
    maxAtaqueEspecial: 20,
    minCura: 5,
    maxCura: 15,
  }

  monstro: Personagem = {
    jogadorAtivo: 0,
    nome: 'Monstro',
    vida: 100,
    ataqueEspecial: 0,
    minAtaque: 6,
    maxAtaque: 12,
    minAtaqueEspecial: 8,
    maxAtaqueEspecial: 16,
    atordoamento: 0
  }

  nomeVencedor: string;
  playerName: string = '';
  score: number = 0;

  turnos: number = 0;
  jogoEmAndamento = true;


  botaoAtaque(){
    this.ataque(this.jogador,this.monstro);
    this.ataqueMonstro();
    this.fimDeTurno();
  }

  botaoAtaqueEspecial(){
    this.ataqueEspecial(this.jogador,this.monstro);
    this.monstro.atordoamento = this.retornaNumeroRandomico(0,2);
    this.ataqueMonstro();
    this.fimDeTurno();
  }

  botaoCura(){
    this.cura(this.jogador);
    this.ataqueMonstro();
    this.fimDeTurno();
  }

  botaoDesistir(){
    this.router.navigateByUrl('home');
  }

  ataqueMonstro(){
    if(this.jogoEmAndamento){
      if(this.monstro.atordoamento){
        this.monstroAtordoado();
      }else if(this.monstro.ataqueEspecial >= 2){
        this.ataqueEspecial(this.monstro,this.jogador);
      }else{
        this.ataque(this.monstro,this.jogador);
      }
    }
  }

  monstroAtordoado(){
    this.monstro.ataqueEspecial++;
    this.monstro.atordoamento = 0;
    this.registraLog(0, 'stun', 'Inimigo atordoado');
  }

  retornaNumeroRandomico(min: number, max: number){        // max não inclusivo
    return Math.floor(Math.random() * (max - min)) + min;
  }

  cura(personagem : Personagem){
    let cura = this.retornaNumeroRandomico(personagem.minCura, personagem.maxCura);
    personagem.vida += cura;
    personagem.vida = personagem.vida > 100 ? 100 : personagem.vida;
    this.registraLog(personagem.jogadorAtivo,'cura',personagem.nome + ' usou "Curar" (+' + cura + ' pontos de vida)');
    personagem.ataqueEspecial++;
  }

  ataque(atacante : Personagem, defensor: Personagem){
    let dano = this.retornaNumeroRandomico(atacante.minAtaque, atacante.maxAtaque);
    defensor.vida -= dano;
    let nomeLog = atacante.jogadorAtivo ? atacante.nome : 'Inimigo';
    this.registraLog(atacante.jogadorAtivo, 'atkBas', nomeLog + ' usou "Ataque Básico" (-' + dano + ' pontos de vida)');
    this.verificaFimDeJogo();
    atacante.ataqueEspecial++;
  }

  ataqueEspecial(atacante : Personagem, defensor: Personagem){
    let dano = this.retornaNumeroRandomico(atacante.minAtaqueEspecial, atacante.maxAtaqueEspecial);
    defensor.vida -= dano;
    let nomeLog = atacante.jogadorAtivo ? atacante.nome : 'Inimigo';
    this.registraLog(atacante.jogadorAtivo, 'atkEsp',nomeLog + ' usou "Ataque Especial" (-' + dano + ' pontos de vida)');
    this.verificaFimDeJogo();
    atacante.ataqueEspecial = 0;
  }

  atkEspCarregando(){
    if (this.jogador.ataqueEspecial < 2){
      return true;
    }
    return false;
  }

  fimDeTurno(){
    this.turnos++;
  }

  verificaFimDeJogo(){
    if(this.jogador.vida <= 0 || this.monstro.vida <= 0){
      this.jogoEmAndamento = false;
      this.verificaVencedor();
      this.ajustaPontosDeVida();
      this.calculaScore();
    }
  }

  verificaVencedor(){
    if (this.monstro.vida <= 0) {
      this.nomeVencedor = this.jogador.nome;
      return 1;
    }else{
      this.nomeVencedor = this.monstro.nome;
      return 0;
    }
  }

  ajustaPontosDeVida(){
    if (this.jogador.vida < 0){
      this.jogador.vida = 0;
    }else if (this.monstro.vida < 0){
      this.monstro.vida = 0;
    }
  }

  calculaScore(){
    this.score = Math.round((this.jogador.vida * 1000) / this.turnos);
  }

  registraLog(personagem: number, acao: string, texto: string){
    //procurar substring e criar funcao para o css capturar o resultado
    //verifica quem esta realizando a ação e a ação realizada
    const novoLog = new LogAcoes();
    novoLog.personagem = personagem;
    novoLog.acao = acao;
    novoLog.texto = texto;
    this.log.unshift(novoLog);
  }

  buscaCorLog(linhaLog: LogAcoes){
    return {
      'acao-log': true,
      'verde-claro': linhaLog.acao == 'cura' && linhaLog.personagem == 1,
      'azul-claro': linhaLog.acao == 'atkBas' && linhaLog.personagem == 1,
      'amarelo-claro': linhaLog.acao == 'atkEsp' && linhaLog.personagem == 1,
      'vermelho-claro': linhaLog.personagem == 0,
      'jogador': linhaLog.personagem == 1,
      'inimigo': linhaLog.personagem == 0,
    };
  }

  nomeInvalido(){
    if (this.playerName == '') {
      return 1;
    }else{
      return 0;
    }
  }

  registraRanking(){
    if (!this.nomeInvalido()) {
      const valorEmitir: Ranking = {
        playerName: this.playerName,
        score: this.score
      };
      console.log(valorEmitir);
      this.service.adicionar(valorEmitir).subscribe(
        (resultado) => {
        console.log(resultado);
        },
        (error) => console.error(error)
      );
      this.router.navigateByUrl('ranking');
    }
  }

  ngOnInit(): void {
  }

}
