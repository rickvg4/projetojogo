import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IniciarJogoComponent } from './iniciar-jogo.component';

describe('IniciarJogoComponent', () => {
  let component: IniciarJogoComponent;
  let fixture: ComponentFixture<IniciarJogoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IniciarJogoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IniciarJogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
