import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RankingService } from '../services/ranking.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private router: Router,
    private service: RankingService
  ){}


  botaoIniciarJogo(){
    this.router.navigateByUrl('iniciar-jogo');
  }

  botaoRegrasJogo(){
    this.router.navigateByUrl('regras-jogo');
  }

  botaoRanking(){
    this.router.navigateByUrl('ranking');
  }

  ngOnInit(): void {
  }

}
